export const userAdapter = (userData) => ({
    ...userData, personId: userData['person_id']
})

export const adaptToUsersTable = (data) => data.map(user => ({
    key: user.id,
    personId: user.personId,
    name: user.name,
    from: user.from,
    lastMessageTime: user.lastMessageTime,
}));


export const adaptToOrdersTable = (data) => data.map(order => ({
    key: order.id,
    orderId: order.id,
    price: order.price,
    currency: order.currency,
    title: order.title,
    status: order.status
}));
