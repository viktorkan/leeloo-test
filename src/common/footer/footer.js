import React from 'react';
import '../../assets/scss/blocks/footer.scss';

const Footer = () => {
  return (
      <footer className='footer'>
        <div className='container'>
          <h2 className='footer__text'>Welcome to the feature!</h2>
        </div>
      </footer>
  )
};

export default Footer;
