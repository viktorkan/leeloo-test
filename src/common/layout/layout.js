import React from 'react';
import Header from "../header";
import Footer from "../footer";

const LayoutWrapper = ({children}) => {
    return (
        <>
            <Header/>
            <div className="container">
                {children}
            </div>
            <Footer/>
        </>
    )
}

export default LayoutWrapper