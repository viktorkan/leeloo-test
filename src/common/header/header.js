import React from 'react';
import '../../assets/scss/blocks/header.scss';

const Header = () => {
    return (
        <header className='header'>
            <div className='container'>
                <img src={'/image/logo.png'} width={200}/>
            </div>
        </header>
    )
}

export default Header;
