import axios from 'axios';
import {notification} from "antd";

const BACKEND_URL = 'https://api.leeloo.ai/api/v1/';
const REQUEST_TIMEOUT = 5000;
const AUTH_TOKEN = 'cmpc3hcajyh3e5ksz6xho9t77flp4pesa300js9y8w9v6xptzj0vlk0halesk27hgqslukjxpepzkmaz6jpfj8zw0x5fwsajezvb';


export const createAPI = () => {
    const api = axios.create({
        baseURL: BACKEND_URL,
        timeout: REQUEST_TIMEOUT,
        // withCredentials: true,
        headers: {'X-Leeloo-AuthToken': AUTH_TOKEN}
    });

    const onSuccess = (response) => response;

    const onFail = (err) => {
        notification.open({
           title: 'Произошла ошибка',
           message: err.message
        });
        throw new Error(err.message);
    };

    api.interceptors.response.use(onSuccess, onFail);

    return api;
};
