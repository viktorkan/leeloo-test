import {combineReducers} from "redux";
import {NameSpace} from "./name-space";
import {reducer as usersReducer} from './users';
import {reducer as userOrdersReducer} from "./user-orders";
import {reducer as appReducer} from "./app";

export default combineReducers({
    [NameSpace.USERS]: usersReducer,
    [NameSpace.USER_ORDERS]: userOrdersReducer,
    [NameSpace.APP]: appReducer
})
