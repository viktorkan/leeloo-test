import {ActionCreator, ActionType, Operation} from "./actions";
import {createAPI} from "../../api";
import MockAdapter from "axios-mock-adapter";


const orders = [
    {
        id: 1,
        price: 2000,
        currency: 'USD',
        title: 'Some title',
        status: 'RESOLVE'
    }
]

const api = createAPI();

describe('Test USER-ORDERS reducer', () => {
    it('Test SET_USER_ORDERS ', () => {
        expect(ActionCreator.setOrders(orders)).toEqual({
            type: ActionType.SET_USER_ORDERS,
            payload: orders
        })
    });

    it('Test SET_USER_ORDERS_LOADING_STATUS', () => {
        expect(ActionCreator.setLoadingStatus(true)).toEqual({
            type: ActionType.SET_USER_ORDERS_LOADING_STATUS,
            payload: true
        })
    });

    it('Test SET_USER_ORDERS_ERROR_STATUS', () => {
        expect(ActionCreator.setErrorStatus(true)).toEqual({
            type: ActionType.SET_USER_ORDERS_ERROR_STATUS,
            payload: true
        })
    });

    it('Test api load-orders', () => {
        const apiMock = new MockAdapter(api);
        const dispatch = jest.fn();
        const loadUser = Operation.loadUser('5dcbd42074edd0000c103e51');

        apiMock
            .onGet('accounts/5dcbd42074edd0000c103e51?include=contactedUsers,orders')
            .reply(200, {
                included: {orders}
            });

        return loadUser(dispatch, jest.fn(), api)
            .then(() => {
                expect(dispatch).toHaveBeenCalledTimes(4);
                expect(dispatch).toHaveBeenNthCalledWith(2, {
                    type: ActionType.SET_USER_ORDERS,
                    payload: orders
                })
            })
    });

});

