import {ActionType} from "./actions";

const initialState = {
    orders: [],
    isUserLoading: false,
    isUserError: false
}


const reducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case ActionType.SET_USER_ORDERS:
            return {...state, orders: payload};
        case ActionType.SET_USER_ORDERS_LOADING_STATUS:
            return {...state, isUserLoading: payload};
        case ActionType.SET_USER_ORDERS_ERROR_STATUS:
            return {...state, isUserError: payload};
        default: return state;
    }
}


export {reducer}
