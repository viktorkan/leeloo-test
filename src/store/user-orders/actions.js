import {ActionCreator as AppActionCreator} from "../app/actions";

const ActionType = {
    SET_USER_ORDERS: 'SET_USER_ORDERS',
    SET_USER_ORDERS_LOADING_STATUS: 'SET_USER_ORDERS_LOADING_STATUS',
    SET_USER_ORDERS_ERROR_STATUS: 'SET_USER_ORDERS_ERROR_STATUS'
};

const ActionCreator = {
  setOrders: (payload) => ({type: ActionType.SET_USER_ORDERS, payload}),
    setLoadingStatus: (payload) => ({type: ActionType.SET_USER_ORDERS_LOADING_STATUS, payload}),
    setErrorStatus: (payload) => ({type: ActionType.SET_USER_ORDERS_ERROR_STATUS, payload})
};


const Operation = {
    loadUser: (id) => (dispatch, _getState, api) => {
        dispatch(ActionCreator.setLoadingStatus(true));
        return api.get(`accounts/${id}?include=contactedUsers,orders`)
            .then(res => {
                dispatch(ActionCreator.setOrders(res.data.included.orders));
                dispatch(ActionCreator.setLoadingStatus(false));
                dispatch(AppActionCreator.changeModal(true));
            })
            .catch(err => {
                dispatch(ActionCreator.setErrorStatus(true));
                throw new Error(err);
            })
    }
};


export {ActionType, Operation, ActionCreator}
