import {ActionType} from "./actions";
import {userAdapter} from "../../utils";

const initialState = {
    users: [],
    usersCount: 0,
    isLoading: false,
    isErrorLoading: false
}


const reducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case ActionType.SET_USERS:
            return {...state, users: payload.data.map(userAdapter)}
        case ActionType.SET_LOADING_STATUS:
            return  {...state, isLoading: payload}
        case ActionType.SET_USERS_COUNT:
            return {...state, usersCount: payload}
        default: return state;
    }
}

export {reducer};
