import {ActionCreator, ActionType, Operation} from "./actions";
import {createAPI} from "../../api";
import MockAdapter from "axios-mock-adapter";

const users = [
    {
        id: 1,
        name: 'Sam Vinchester',
        from: 'FACEBOOK',
        lastMessageTime: '22-10-2020'
    }
]

const api = createAPI();

describe('Test USERS redicer', () => {
    it('Test SET_USERS ', () => {
        expect(ActionCreator.setUsers(users)).toEqual({
            type: ActionType.SET_USERS,
            payload: users
        })
    });
    it('Test SET_LOADING_STATUS ', () => {
        expect(ActionCreator.setLoadingStatus(true)).toEqual({
            type: ActionType.SET_LOADING_STATUS,
            payload: true
        })
    });
    it('Test SET_USERS_COUNT ', () => {
        expect(ActionCreator.setUsersCount(21)).toEqual({
            type: ActionType.SET_USERS_COUNT,
            payload: 21
        })
    });
    it('Test OPERATION loadUsers ', () => {
        const apiMock = new MockAdapter(api);
        const dispatch = jest.fn();
        const loadUsers = Operation.loadUsers();

        const data = {
            data: users,
            meta: {totalCount: 21}
        }

        apiMock.onGet('accounts?limit=50&offset=0')
            .reply(200, data)

        return loadUsers(dispatch, jest.fn(), api)
            .then(() => {
                expect(dispatch).toHaveBeenCalledTimes(4);
            })
    });
});