
const ActionType = {
    SET_USERS: 'SET_USERS',
    SET_LOADING_STATUS: 'SET_LOADING_STATUS',
    SET_ERROR_STATUS: 'SET_ERROR_STATUS',
    SET_USERS_COUNT: 'SET_USERS_COUNT'
}

const ActionCreator = {
    setUsers: (payload) => ({type: ActionType.SET_USERS, payload}),
    setLoadingStatus: (payload) => ({type: ActionType.SET_LOADING_STATUS, payload}),
    setErrorStatus: (payload) => ({type: ActionType.SET_ERROR_STATUS, payload}),
    setUsersCount: (payload) => ({type: ActionType.SET_USERS_COUNT, payload})
}

const Operation = {
    loadUsers: (offset = 0, limit = 50) => (dispatch, _getState, api) => {

        dispatch(ActionCreator.setLoadingStatus(true));
        return api.get(`accounts?limit=${limit}&offset=${offset}`)
            .then(res => {
                dispatch(ActionCreator.setUsers(res.data));
                dispatch(ActionCreator.setUsersCount(res.data.meta.totalCount));
                dispatch(ActionCreator.setLoadingStatus(false));
            })
            .catch(err => {
                dispatch(ActionCreator.setErrorStatus(true));
                throw new Error(err)
            })
    }
};


export {ActionType, ActionCreator, Operation}
