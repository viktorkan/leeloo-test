const ActionType = {
    CHANGE_MODAL_OPEN: 'CHANGE_MODAL_OPEN'
}

const ActionCreator = {
    changeModal: (payload) => ({type: ActionType.CHANGE_MODAL_OPEN, payload})
};


export {ActionType, ActionCreator}
