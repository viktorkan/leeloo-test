import {ActionType, ActionCreator} from "./actions";

it('APP reducer works correctly', () => {
    expect(ActionCreator.changeModal(true)).toEqual({
        type: ActionType.CHANGE_MODAL_OPEN,
        payload: true
    })
});