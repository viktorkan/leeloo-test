import {ActionType} from "./actions";

const initialState = {
    modalOpen: false
};

const reducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case ActionType.CHANGE_MODAL_OPEN:
            return {...state, modalOpen: payload};
        default: return state;
    }
};

export {reducer};
