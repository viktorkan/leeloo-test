import React, {useEffect, useState} from 'react';
import LayoutWrapper from "../../common/layout";
import {connect} from 'react-redux';
import {Operation} from "../../store/users/actions";
import {NameSpace} from "../../store/name-space";
import {Modal} from "antd";
import {ActionCreator as AppActionCreator} from "../../store/app/actions";
import {UsersTable} from "../../components/table";
import {OrdersTable} from "../../components/table/table";

const HomePage = ({users, isLoading, total, loadUsers, isModalOpen, changeModalOpen, orders}) => {

    const handleModalButtonClick = () => changeModalOpen(false);
    useEffect(() => {loadUsers()}, []);
  return (
      <LayoutWrapper>
        <UsersTable data={users} isLoading={isLoading} total={total} loadData={loadUsers}/>
          <Modal title='User information'
                 visible={isModalOpen}
                 centered
                 onOk={handleModalButtonClick}
                 width={'70%'}
          >
              <OrdersTable data={orders}/>
          </Modal>
      </LayoutWrapper>
  )
};


const mapStateToProps = (state) => ({
    isLoading: state[NameSpace.USERS].isLoading,
    users: state[NameSpace.USERS].users,
    total: state[NameSpace.USERS].usersCount,
    isModalOpen: state[NameSpace.APP].modalOpen,
    orders: state[NameSpace.USER_ORDERS].orders,
    isError: state[NameSpace.USERS].isError
});

const mapDispatchToProps = (dispatch) => ({
   loadUsers: (...params) => dispatch(Operation.loadUsers(...params)),
    changeModalOpen: (payload) => dispatch(AppActionCreator.changeModal(payload))
});


export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
