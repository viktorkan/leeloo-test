import React from 'react';
import {TABLE_USER_INFO_COLUMNS, TABLE_USERS_COLUMNS} from "./data/table-data";
import {adaptToOrdersTable, adaptToUsersTable} from "../../utils";
import {Table} from "antd";


const UsersTable = ({total, data, isLoading, loadData}) => {
    const handleTableChange = ({current, pageSize}) => {
        const offset = (pageSize * current) - pageSize;
        const limit = total - offset < pageSize ? total - offset - 1 : pageSize;
        loadData(offset, limit);
    };

    return (
        <Table columns={TABLE_USERS_COLUMNS}
               pagination={{pageSize: 50, showSizeChanger: false, total}}
               dataSource={adaptToUsersTable(data)}
               loading={isLoading}
               onChange={handleTableChange}
        />
    )
}

const OrdersTable = ({data, isLoading}) => {
    return (
        <div>
            {!!data.length && <Table scroll={{ x: 500 }} columns={TABLE_USER_INFO_COLUMNS}
                                   dataSource={adaptToOrdersTable(data)}
            /> || <h2>Нет счетов, увы:(</h2>}
        </div>

    )
};

export {UsersTable, OrdersTable}
