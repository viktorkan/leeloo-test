import React from 'react';
import {OrdersTable, UsersTable} from "./table";
import renderer from 'react-test-renderer';
import thunk from "redux-thunk";
import configureStore from "redux-mock-store";
import {Operation as APIAction} from "../../store/users/actions";
import {Provider} from "react-redux";
import {NameSpace} from "../../store/name-space";

const middleWares = [thunk];
const mockStore = configureStore(middleWares);
APIAction.loadUsers = () => (dispatch) => dispatch(jest.fn());

const mockState = {
    [NameSpace.USERS]: {
        users: [{
            id: 1,
            personId: 2,
            name: 'Thomas',
            from: 'FACEBOOK',
            lastMessageTime: '22-01-2020'
        }],
        usersCount: 0,
        isLoading: true,
        isErrorLoading: false
    },
    [NameSpace.APP]: {
        modalOpen: false
    },
    [NameSpace.USER_ORDERS]: {
        orders: [{
            id: 1,
            price: 200,
            currency: 'USD',
            title: 'SOME TITLE',
            status: 'RESOLVE'
        }],
        isUserLoading: true,
        isUserError: false
    }
};

Object.defineProperty(window, "matchMedia", {
    writable: true,
    value: jest.fn().mockImplementation(query => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
    }))
});


describe('Snapshot tests Table components', () => {
    const store = mockStore(mockState);
    it('Snapshot UsersTable', () => {
        const tree = renderer.create(
            <Provider store={store}>
                <UsersTable
                    total={200}
                    data={[{
                        id: 1,
                        personId: 2,
                        name: 'Thomas',
                        from: 'FACEBOOK',
                        lastMessageTime: '22-01-2020'
                    }]}
                    isLoading={false}
                />
            </Provider>
        ).toJSON();

        expect(tree).toMatchSnapshot();
    });

    it('Snapshot OrdersTable', () => {
        const tree = renderer.create(
            <Provider store={store}>
                <OrdersTable
                    data={[{
                        id: 1,
                        price: 200,
                        currency: 'USD',
                        title: 'SOME TITLE',
                        status: 'RESOLVE'
                    }]}
                    isLoading={false}
                />
            </Provider>
        ).toJSON();

        expect(tree).toMatchSnapshot();
    });

});