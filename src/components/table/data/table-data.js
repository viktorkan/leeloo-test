import React from 'react';
import MoreButton from "../../more-button";

export const TABLE_USERS_COLUMNS = [
        {
            title: '#',
            dataIndex: 'personId',
            key: 'personId',
            responsive: ['xs', 'sm', 'md', 'lg', 'xl', 'xxl'],
            ellipsis: true,
        },
        {
            title: 'Имя',
            dataIndex: 'name',
            key: 'name',
            responsive: ['sm']
        },
        {
            title: 'Мессенджер',
            dataIndex: 'from',
            key: 'from',
            responsive: ['sm']
        },
        {
            title: 'Время последнего заказа',
            dataIndex: 'lastMessageTime',
            key: 'lastMessageTime',
            responsive: ['sm']
        },
        {
            title: 'Дополнительная информация',
            key: 'moreInformation',
            responsive: ['xs', 'sm', 'md', 'lg', 'xl', 'xxl'],
            render: (user) => <MoreButton personId={user.key}>Подробнее</MoreButton>
        },
    ];


export const TABLE_USER_INFO_COLUMNS = [
    {
        title: 'ID заказа',
        dataIndex: 'orderId',
        key: 'orderId',
        ellipsis: true,
        fixed: 'left'
    },
    {
        title: 'Цена',
        dataIndex: 'price',
        key: 'price',
        ellipsis: true,
    },
    {
        title: 'Валюта',
        dataIndex: 'currency',
        key: 'currency',
        ellipsis: true,
    },
    {
        title: 'Способ оплаты',
        dataIndex: 'title',
        key: 'title',
        ellipsis: true,
    },
    {
        title: 'Статус',
        dataIndex: 'status',
        key: 'status',
        ellipsis: true,
    },
];
