import React from 'react';
import renderer from 'react-test-renderer';
import MoreButton from './index';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {Operation as APIAction} from "../../store/user-orders/actions";
import thunk from 'redux-thunk';
import {NameSpace} from "../../store/name-space";

const middleWares = [thunk];
const mockStore = configureStore(middleWares);
APIAction.loadUser = () => (dispatch) => dispatch(jest.fn());

const mockState = {
    [NameSpace.USERS]: {
        users: [{
            id: 1,
            personId: 2,
            name: 'Thomas',
            from: 'FACEBOOK',
            lastMessageTime: '22-01-2020'
        }],
        usersCount: 0,
        isLoading: true,
        isErrorLoading: false
    },
    [NameSpace.APP]: {
        modalOpen: false
    },
    [NameSpace.USER_ORDERS]: {
        orders: [{
            id: 1,
            price: 200,
            currency: 'USD',
            title: 'SOME TITLE',
            status: 'RESOLVE'
        }],
        isUserLoading: true,
        isUserError: false
    }
};

it('Snapshot test MoreButton ', () => {
    const store = mockStore(mockState);
    const tree = renderer.create(
        <Provider store={store}>
            <MoreButton/>
        </Provider>
    ).toJSON();

    expect(tree).toMatchSnapshot()
});