import React from 'react';
import {Button} from "antd";
import {Operation} from "../../store/user-orders/actions";
import {connect} from 'react-redux';

const MoreButton = ({personId, loadingUser}) => {
    const buttonClickHandler = () => {
        loadingUser(personId)
    };

    return (
        <Button block onClick={buttonClickHandler}>Подробнее</Button>
    )
};

const mapDispatchToProps = (dispatch) => ({
    loadingUser: (id) => dispatch(Operation.loadUser(id))
});

export default connect(null, mapDispatchToProps)(MoreButton);
