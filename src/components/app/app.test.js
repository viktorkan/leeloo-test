import React from 'react';
import renderer from 'react-test-renderer';
import App from "./app";
import {Provider} from "react-redux";
import {NameSpace} from "../../store/name-space";
import configureStore from 'redux-mock-store';
import {Operation as APIAction} from "../../store/users/actions";
import thunk from 'redux-thunk';

const middleWares = [thunk];
const mockStore = configureStore(middleWares);
APIAction.loadUsers = () => (dispatch) => dispatch(jest.fn());

const mockState = {
  [NameSpace.USERS]: {
      users: [{
          id: 1,
          personId: 2,
          name: 'Thomas',
          from: 'FACEBOOK',
          lastMessageTime: '22-01-2020'
      }],
      usersCount: 0,
      isLoading: true,
      isErrorLoading: false
  },
  [NameSpace.APP]: {
      modalOpen: false
  },
  [NameSpace.USER_ORDERS]: {
      orders: [{
          id: 1,
          price: 200,
          currency: 'USD',
          title: 'SOME TITLE',
          status: 'RESOLVE'
      }],
      isUserLoading: true,
      isUserError: false
  }
};

Object.defineProperty(window, "matchMedia", {
    writable: true,
    value: jest.fn().mockImplementation(query => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
    }))
});


it('Snapshoot test APP', () => {
    const store = mockStore(mockState);

    const tree = renderer.create(
        <Provider store={store}>
            <App/>
        </Provider>, {
            createNodeMock: () => document.createElement('div')
        }
    ).toJSON();

    expect(tree).toMatchSnapshot()
});
