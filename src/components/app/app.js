import React from 'react';
import {BrowserRouter as Router, Switch, Route, Redirect} from "react-router-dom";
import HomePage from "../../pages/home-page";

const PUBLIC_ROUTES = {
    HOME: '/'
};

const App = () => {
    return (
        <Router>
            <Switch>
                <Route path={PUBLIC_ROUTES.HOME} exact component={HomePage} />
            </Switch>
        </Router>
    )
}

export default App;