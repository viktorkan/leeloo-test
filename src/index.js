import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';
import {createStore, applyMiddleware} from "redux";
import {Provider} from 'react-redux';
import thunk from "redux-thunk";
import rootReducer from "./store";

import 'antd/dist/antd.css';
import './assets/scss/index.scss';

import {createAPI} from "./api";

const api = createAPI();
const store = createStore(rootReducer, applyMiddleware(thunk.withExtraArgument(api)));


ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.querySelector('.root')
)
